$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "resqovery/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "resqovery"
  s.version     = Resqovery::VERSION
  s.authors     = ["Arash Karimzadeh"]
  s.email       = ["arash.k@yoyobutton.com"]
  s.homepage    = "https://github.com/tellafriend/resqovery"
  s.summary     = "DB recovery for Resque"
  s.description = "DB recovery system for Resque"

  s.files = Dir["{app,config,db,lib}/**/*"] + ["MIT-LICENSE", "Rakefile", "README.rdoc"]

  s.add_dependency "rails"

  s.add_development_dependency "rspec-rails"
  s.add_development_dependency "sqlite3"
end
