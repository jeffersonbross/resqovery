class CreateResqueJobs < ActiveRecord::Migration
  def change
    create_table :resque_jobs do |t|
      t.string    :classname
      t.text      :args
      t.boolean   :is_executed, :default => false

      t.timestamps
    end
  end
end
