= Resqovery "Resque Recovery"

It adds a method <tt>safe_enqueue</tt>

== Installation

In <b>Rails 3</b>, add following line to your Gemfile and run the <tt>bundle</tt> command.

  gem "resqovery"

In <b>Rails 2</b>, add following line to your environment.rb file.

  config.gem "resqovery"

== Getting Started

There couple of generators. you can run <tt>rails g reqovery:install -h</tt> to see what they do

  resqovery:install
  resqovery:migration

In case you want to rename default model/table for storing jobs (default name is "ResqueJob"):

  rails g resqovery:install -m [MODEL_NAME]
  rails g resqovery:install -m backup_job

After creating migration and running them by <tt>rake db:migrate</tt>, you can create jobs as before using:

  Resque.enqueue Archive, "master"

or using following one which store the job in DB before calling resque

  Resque.safe_enqueue Archive, "master"

