class ResqueJob < ActiveRecord::Base
  
  serialize :args
  attr_accessible :classname, :args, :is_executed

  def is_executed?
    is_executed
  end

end
