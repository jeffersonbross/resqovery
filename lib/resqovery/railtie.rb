require 'resqovery'

module Resqovery
   
  class << self 
    attr_accessor :defaults;

    def configure
      yield(defaults)
    end

  end
  Resqovery.defaults = { :model_name => 'ResqueJob' }

  if Rails.version =~ /^3\./
    class Railtie < Rails::Railtie
      initializer 'resqovery.configure' do |app|
        if app.config.respond_to?(:resqovery_defaults)
          Resqovery.defaults.merge!(app.config.resqovery_defaults)
        end
      end
    end
  end

end
