require 'resqovery/railtie' if defined?(Rails)

module Resque
  
  def self.safe_enqueue(*args, &block)
    classname = *args[0].to_s
    params = *args[1..-1]
    id = Resqovery.defaults[:model_name].constantize.create( {:classname => classname , :args => params, :is_executed => false} ).id
    self.enqueue(*args+[id], &block)
  end
   
end
