require 'rails/generators'

module Resqovery

  class InstallGenerator < Rails::Generators::Base
  
    class_option :model , :type => :string, :desc => 'Model name to store jobs'  , :default => 'resque_job'  , :aliases => '-m'

    desc 'Run all generators which create model file and migration'

    source_root File.expand_path('../templates', __FILE__)

    def generate_migration
      generate "resqovery:migration #{model}"
    end

    def generate_app
      template "models/resque_job.rb.erb",  "#{Rails.root}/app/models/#{model}.rb"
    end

    protected
      def model
        options.model.downcase.singularize
      end

  end

end
